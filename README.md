***
### ENTORNO DE MOODLE DOCKERIZADO

Esto trata de ser un entorno de moodle para instalar en un pc y poder desarrollar.

Se compone de dos contenedores:

- Apache + PHP7.1 : Servidor web y que tiene persistencia con el host desde el contenedor en la carpeta `codigo`.

- Base de datos: Un contenedor con MariaDb.

#### PASOS DE INSTALACIÓN PARA CONSEGUIR TENER UN ENTORNO DE DESARROLLO LOCAL


1º) Para descargar este proyecto al tener submodulos usar:

`git clone --recurse-submodules https://gitlab.com/isaacDiez/moodle-dockerizado.git`

2º) Escoger la rama de desarrollo (ej: MOODLE_39_STABLE) [Versiones Moodle](https://docs.moodle.org/all/es/dev/Historia_de_las_versiones) y [Github Official Moodle](https://github.com/moodle/moodle)

`cd moodle-dockerizado/codigo/moodle`

`git checkout MOODLE_39_STABLE`

3º) Iniciamos nuestros contenedores (web y databasemoodle)

`cd ../..`

`docker-compose up`

4º) Desde un navegador comenzamos la instalación grafica de moodle

`http://localhost:8086`

5º) Para esta instalacion debemos saber que la base de datos usada es POSTGRESQL y los datos de inicio son: 

```
POSTGRES_DB=moodle_db_develop

POSTGRES_PASSWORD=pass

POSTGRES_USER=admin
```

6º) Tenemos que cambiar los permisos de la carpeta de desarrollo del codigo para poder editarlos.

`chmod 777  moodle-dockerizado/codigo/moodle`

-------

